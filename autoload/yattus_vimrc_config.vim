"=============================================================================
" vimrc --- Entry file for vim
" Copyright (c) 2021 Yattara
" Author: Yattara
" URL: 
" License: MIT
"=============================================================================

function! yattus_vimrc_config#before() abort
    nnoremap qq :q<CR>
    vnoremap qq <ESC>:q<CR>
    nnoremap <leader>; :q!<CR>
    nmap <C-x> <C-x>

    let g:mapleader  = ';'
    " after this line, when you using <leader> to defind key bindings
    " the leader is ,
    " for example:
    nnoremap <leader>w :w<cr>
    " this mapping means using `,w` to save current file.

	" Delete buffer
	nmap qb :bd<CR>

	" Let g:spacevim_windows_leader = ';'
	let g:spacevim_enable_cursorcolumn = 1
	let g:spacevim_enable_statusline_mode = 1
	let g:spacevim_expand_tab = 4 
	let g:spacevim_enable_ycm = 0
	let g:spacevim_default_indent = 4
	let g:spacevim_vim_help_language = 'fr'
    let g:spacevim_disabled_plugins = ['deoplete', 'deoplete-vim-lsp', 'coc']

    " DISPLAY datetime in Vim
    nnoremap <F5> "=strftime("%c")<CR>P
    inoremap <F5> <C-R>=strftime("%c")<CR>

    " Replace word all occurences off word
	nnoremap <Leader>R :call Replace(0, 0, input('Replace '.expand('<cword>').' with: '))<CR>
    " Replace word all occurences off word
	nnoremap <Leader>rw :call Replace(0, 1, input('Replace '.expand('<cword>').' with: '))<CR>
    " Replace word with confirmation
	nnoremap <Leader>rc :call Replace(1, 0, input('Replace '.expand('<cword>').' with: '))<CR>

	set ruler
	set ttyfast                                 " terminal acceleration

	set tabstop=4                               " 4 whitespaces for tabs visual presentation
	set shiftwidth=4                            " shift lines by 4 spaces
	set smartcase
	set ignorecase    " case insensitive searching (unless specified)
	set smarttab                                " set tabs for a shifttabs logic
	set expandtab                               " expand tabs into spaces
	set autoindent                              " indent when moving to the next line while writing code

    set foldmethod=indent   " indent
    set foldnestmax=10
    " set nofoldenable
    " set foldlevel=2

    " let the language server automatically handle folding for you
    " set foldmethod=expr
      " \ foldexpr=lsp#ui#vim#folding#foldexpr()
      " \ foldtext=lsp#ui#vim#folding#foldtext()

    " Tab completion insert mode for asyncomplete
    inoremap <expr> <Tab>   pumvisible() ? "\<C-n>" : "\<Tab>"
    inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
    inoremap <expr> <cr>    pumvisible() ? asyncomplete#close_popup() : "\<cr>"

	set colorcolumn=80
	highlight ColorColumn ctermbg=234

	if has('autocmd')
			augroup coloroverride
					autocmd!
                    "autocmd ColorScheme * highlight LineNr ctermbg=234  ctermfg=238 guifg=238 " Override LineNr rgb=0,0,0
					autocmd ColorScheme * highlight CursorLineNr  ctermbg=234 ctermfg=green guifg=green  " Override CursorLineNr
			augroup END
	endif
	silent! colorscheme eldar " Custom color scheme

	" Color colonne number
	" highlight LineNr ctermbg=234  ctermfg=238 "rgb=0,0,0

	set cursorline                              " shows line under the cursor's line
	highlight cursorline ctermbg=234 cterm=BOLD

	set showmatch                               " shows matching part of bracket pairs (), [], {}

	set enc=utf-8                             " utf-8 by default"])

	set visualbell    " stop that ANNOYING beeping
	set showcmd
	set autoread
	set backspace=2
	set nobackup 	                            " no backup files
	set nowritebackup                           " only in case you don't want a backup file while editing
	set noswapfile 	                            " no swap files

	set backspace=indent,eol,start              " backspace removes all (indents, EOLs, start) What is start?

	set history=50

	set clipboard=unnamed                       " use system clipboard

	set exrc                                    " enable usage of additional .vimrc files from working directory
	set secure                                  " prohibit .vimrc files to execute shell, create files, etc...

	" Auto resize Vim splits to active split
	set winwidth=104
	set winheight=12
	set winminwidth=12
	set winminheight=5
	set winheight=999

	" Le curseur ne peut pas clignoter
	set gcr=a:block-blinkon0

	" Interdire l'affichage de la barre de défilement
	set guioptions-=l
	set guioptions-=L
	set guioptions-=r
	set guioptions-=R

	" Interdire l'affichage des menus et des barres d'outils

	set guioptions-=m
	set guioptions-=T

	nmap <leader>rh :res -10<CR>
	nmap <leader>hr :res +10<CR>
	nmap <leader>rv  :vertical res -10<CR>
	nmap <leader>vr :vertical res +10<CR>

	" split vertical
	nmap <leader>sv :vsplit<CR>
	nmap <leader>sh :split<CR>

	"HTML Editing
	set matchpairs+=<:>

	" Treat <li> and <p> tags like the block tags they are
	let g:html_indent_tags = 'li\|p'

	" ================ Scrolling ========================
	set scrolloff=8         "Start scrolling when we're 8 lines away from margins
	set sidescrolloff=15
	set sidescroll=1

	" Trigger autoread when changing buffers or coming back to vim in terminal.
	au FocusGained,BufEnter * :silent! !

	"******** BUFFER RACCOURCI ***********
	" buffer next
	nmap <C-o> :bn<CR>
	imap <C-o> <ESC>:bn<CR>

	" buffer last
	nmap <C-u> :bp<CR>
	imap <C-u> <ESC>:bp<CR>


	" Appelez l'arbre gundo
	nnoremap <leader>ud :GundoToggle<CR>

	nnoremap <silent> <leader>e :noh<cr> " Stop highlight after searching

	" Quicker window movement
	nnoremap <C-j> <C-w>j
	nnoremap <C-k> <C-w>k
	nnoremap <C-h> <C-w>h
	nnoremap <C-l> <C-w>l

	" tab length exceptions on some file types
	autocmd FileType html setlocal shiftwidth=4 tabstop=4 softtabstop=4
	autocmd FileType htmldjango setlocal shiftwidth=4 tabstop=4 softtabstop=4
	autocmd FileType javascript setlocal shiftwidth=4 tabstop=4 softtabstop=4

    " Vim note directories
    let g:notes_directories = ['~/Code/Notes']

    " VIM-JAVASCRIPT
    let g:javascript_plugin_jsdoc = 1
    let g:javascript_plugin_ngdoc = 1

	" documentation
    " let g:deoplete#enable_at_startup = 1

    " Code completions for typescript
    " let g:coc_global_extensions = [ 'coc-tsserver' ]

	" syntastic
	nmap <silent> <leader>er :SyntasticCheck # <CR> :bp <BAR> bd #<CR>

	let g:syntastic_always_populate_loc_list=1
	let g:syntastic_auto_loc_list=1
	" let g:syntastic_enable_signs=1
	let g:syntastic_check_on_wq=0
	let g:syntastic_aggregate_errors=1
	let g:syntastic_loc_list_height=5
	" custom icons (enable them if you use a patched font, and enable the previous 
	" setting)

	" let g:syntastic_error_symbol = '✗'
    let g:syntastic_error_symbol = "☠"
	let g:syntastic_warning_symbol = '⚠'
    let g:syntastic_style_error_symbol = "☢"
	" let g:syntastic_style_error_symbol ='⚠'  " '✗'
	let g:syntastic_style_warning_symbol = '⚠'
    let g:syntastic_python_checkers=['flake8', 'pydocstyle', 'python', 'pep8']
    let g:flake8_error_marker= "☠"     " set error marker to 'EE'
    let g:flake8_warning_marker= "⚠"   " set warning marker to 'WW'
    let g:flake8_pyflake_marker=''     " disable PyFlakes warnings
    let g:flake8_complexity_marker='☢'  " disable McCabe complexity warnings
    let g:flake8_naming_marker=''      " disable naming warnings


	" DragVisuals ------------------------------

	" mappings to move blocks in 4 directions
	vmap <expr> H DVB_Drag('left')
	vmap <expr> L DVB_Drag('right')
	vmap <expr> J DVB_Drag('down')
	vmap <expr> K DVB_Drag('up')
	" mapping to duplicate block
	vmap <expr> D DVB_Duplicate()

	let g:multi_cursor_use_default_mapping=0

	" Default mapping
	let g:multi_cursor_start_word_key      = '<C-n>'
	let g:multi_cursor_select_all_word_key = '<A-n>'
	let g:multi_cursor_start_key           = 'g<C-n>'
	let g:multi_cursor_select_all_key      = 'g<A-n>'
	let g:multi_cursor_next_key            = '<C-n>'
	let g:multi_cursor_prev_key            = '<C-p>'
	let g:multi_cursor_skip_key            = '<C-j>'
	let g:multi_cursor_quit_key            = '<Esc>'


	" NERDTREE
	let NERDTreeIgnore=['\.pyc$', '\.pyo$', '__pycache__$']     " Ignore files in NERDTree
	let g:NERDTreeDirArrowExpandable = '▸'
	let g:NERDTreeDirArrowCollapsible = '▾'
    let g:NERDTreeWinPos="right"
	map <F3> :NERDTreeToggle<CR>
	let g:spacevim_filemanager = 'NerdTree'
	let g:spacevim_enable_vimfiler_welcome = 0

	" When reading a buffer (after 1s), and when writing (no delay).
	call neomake#configure#automake('rw', 1000)

    let g:github_dashboard = { 'username': 'yattdev', 'password': $GITHUB_TOKEN }
    let g:gista#client#default_username = 'yattdev'
endfunction

" *************CONFIGUARTION DE DEFAULT VIM***********
function! Replace(confirm, wholeword, replace)
		wa
		let flag = ''
		if a:confirm
				let flag .= 'gec'
		else
				let flag .= 'ge'
		endif
		let search = ''
		if a:wholeword
				let search .= '\<' . escape(expand('<cword>'), '/\.*$^~[') . '\>'
		else
				let search .= expand('<cword>')
		endif
		let replace = escape(a:replace, '/\&~')
		execute '%s/' . search . '/' . replace . '/' . flag . '| update'
endfunction
